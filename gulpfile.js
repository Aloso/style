"use strict";
const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');
const cleanCSS    = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
// const sourcemaps  = require('gulp-sourcemaps');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', gulp.series(function() {
    return gulp.src("src/*.scss")
        // .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({
            inline: ['none'],
            level: {
                2: {
                    restructureRules: true, // must be run before autoprefixer
                },
            },
        }))
        .pipe(autoprefixer({
            flexbox: "no-2009",
            cascade: false
        }))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest("public"))
        .pipe(browserSync.stream());
}));

// Static Server + watching scss/html files
gulp.task('static', gulp.series(['sass'], function() {
    browserSync.init({
        server: {
            baseDir: ["../front_end/public", "./public"],
            serveStaticOptions: {
                extensions: ["html"]
            },
        },
    });

    gulp.watch("src/*.scss", gulp.series(['sass']));
    gulp.watch("public/crates/*.html").on('change', browserSync.reload);
}));

// Real Server + watching scss/html files
gulp.task('server', gulp.series(['sass'], function() {
    const serve = require("serve-static")("public");
    browserSync.init({
        proxy: "127.0.0.1:32531",
        open: false,
        middleware: [
            function (req, res, next) {
                if (/\.css/.test(req.url)) {
                    serve(req, res, next);
                } else {
                    next();
                }
            },
        ],
    });

    gulp.watch("src/*.scss", gulp.series(['sass']));
    gulp.watch("public/crates/*.html").on('change', browserSync.reload);
}));


gulp.task('default', gulp.series(['server']));

