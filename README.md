# Sass sandbox for crates.rs

This is just the CSS for the crates.rs project. There are 3 sample pages included in the repository.

## Usage

Requires Node.js. Install:

```sh
npm i
```

Run:

```sh
npm start
```

Open:

  * http://localhost:3000/
  * http://localhost:3000/no-std.html
  * http://localhost:3000/bitflags.html

and then edit `*.scss` files in `src/`. When `gulp` is running, the CSS will update and reload automatically.

## Advanced usage

Assuming you have full checkout of the entire project, you can start the full crates server on localhost with:

```sh
( cd ../server; cargo run ) &
```

and then live-edit (only) `index.scss` used by the server:

```sh
npm run server
```

## License

This project: Apache/MIT.
The Fira font: SIL Open Font License.
